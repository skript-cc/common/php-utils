# PHP utils

PHP utilities and monorepo experiment.

## Goals

What are the goals of this repository?

1. **Learn.** Writing code is a way of learning by doing. The new code that you write
   might not be extremely useful in the end, but probably the act of writing
   leads to new insights. Writing utility functions in isolation makes you
   reflect a little bit more on what you're doing. For example because you have
   to think about the added value of an utility, does it make sense or not?
2. **Serve as a reference.** Sometimes you just need a reminder of how something
   can be implemented. It's nice to lookup something without spitting through
   previous projects.
3. **Bundle one-off functions** which are normally copied from project to project.
   The DRY part. You know. The holy grail of re-usability. Sometimes you want to
   write things once and use them at various places.

What are the non-goals?

- It doesn't aim to be a feature complete library 

## Guidelines for adding utilies

When to add an 'utility'? What makes sense to do and what not?

It makes sense

- when the utility simplifies a complex operation
- when the utility makes an operation more descriptive

It does not make sense

- when the utility duplicates something that's already possible. (This might be
  oke if the utility makes the operation more descriptive. In that case, try to
  create a simple alias.)

## Monorepo setup

Architecture: each package is a folder in the packages/ dir with an own
composer.json file. Each package can be connected to its own gitremote. That
makes packagist happy too!

Development can be done from the monorepo. The monorepo builder has a split
command that syncs the changes to the individual git remotes (this can be 
automated with a ci script).

### Monorepo builder

The monorepo builder thing basicly has two operations: merge and split.

- Merge: merges sections as specified in monorepo-builder.yml from each package's
  composer file into the root composer.json file.
- Split: syncs each package to its own remote repository

Another useful one is release.

- Release: bumps version of interdependent packages, adds tags, updates main 
  changelog. Does push changes to the main repo, but you need to run split again
  to also push the changes to the subpackage remotes.

## Notes

PHP documentor seem to have some issues with new language features. It trips over
the multiple import declaration in filesystem/src/functions.php and the nullable
type hint.

It also displays all php warnings and notices when running on the command line
(but this might also have to do with my php settings).