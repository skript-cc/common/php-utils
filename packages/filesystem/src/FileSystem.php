<?php

declare(strict_types=1);

namespace Skript\Utils;

require_once __DIR__.'/functions.php';

use Skript\Utils\Iface\StaticFunctionProxyTrait;

class FileSystem
{
    use StaticFunctionProxyTrait;
    protected static $functionNamespace = __NAMESPACE__.'\\FileSystem';
}