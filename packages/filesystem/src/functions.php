<?php

declare(strict_types=1);

namespace Skript\Utils\FileSystem;

use function Skript\Utils\Path\{isAbsolute, join, relative};

/**
 * Creates a symlink to a specific path, at a specific location
 *
 * @param string $targetPath The path to link to
 * @param string $linkPath The location at which to place the link
 */
function symlink(string $targetPath, ?string $linkPath = null): bool
{
    $cwd = getcwd();
    
    if (!isAbsolute($targetPath)) {
        $targetPath = join($cwd, $targetPath);
    }
    
    if ($linkPath) {
        if (!isAbsolute($linkPath)) {
            $linkPath = join($cwd, $linkPath);
        }
        $linkName = basename($linkPath);
    } else {
        $linkName = basename($targetPath);
        $linkPath = join($cwd, $linkName);
    }
    
    if (file_exists($linkPath)) {
        throw new \Exception("Failed to create symlink '$linkPath': file exists");
        return false;
    }
    
    chdir(dirname($linkPath));
    $success = \symlink(
        relative(dirname($linkPath), $targetPath),
        $linkName
    );
    chdir($cwd);
    
    return $success;
}

/**
 * Removes file or directory (recursively)
 *
 * @param string $path The path to remove
 * @param bool $recursive Whether to recursively delete the content of the given path
 */
function rm(string $path, bool $recursive = true): bool
{
    // delete regular files and symlinks
    if (!is_dir($path) || is_link($path)) {
        return file_exists($path) ? unlink($path) : false;
    }
    
    // recursively delete files and directories
    if ($recursive) {
        $files = array_diff(scandir($path), ['.', '..']);
        foreach ($files as $file) {
            rm(join($path, $file), $recursive);
        }
    }
    
    // delete directory
    return rmdir($path);
}

/**
 * Creates dir with parents. Fails silently when the location already exists.
 *
 * @param string $path The path to the directory to create
 * @param bool $overwrite Whether to overwrite existing files or directories
 * @param int $mode Access mode
 * @param bool $parents Whether to create intermediate parents or not
 */
function mkdir(
    string $path,
    bool $overwrite = false,
    int $mode = 0755,
    bool $parents = true
): bool {
    $fileExists = file_exists($path);
    if ($fileExists && !$overwrite) {
        return false;
    } elseif ($fileExists && $overwrite) {
        rm($path, true);
    }
    return \mkdir($path, $mode, true);
}