<?php

declare(strict_types=1);

namespace Skript\Utils\Iface;

/**
 * A convenience interface that maps static methods to functions
 */
trait StaticFunctionProxyTrait
{
    public static function __callStatic($name, $arguments)
    {
        $func = isset(self::$functionNamespace) 
            ? self::$functionNamespace.'\\'.$name
            : $name;
        if (!function_exists($func)) {
            throw new \BadMethodCallException("The method $name does not exists.");
        }
        return call_user_func_array($func, $arguments);
    }
}
