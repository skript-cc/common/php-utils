<?php

declare(strict_types=1);

namespace Skript\Utils\_Array;

use PHPUnit\Framework\TestCase;

use function Skript\Utils\_Array\{
    pick,
    pickValues,
    pickWithDefaults,
    pickValuesWithDefaults
};

final class FunctionsTest extends TestCase
{
    /**
     * @testdox The result from pick should only contain the elements which correspond to the given keys
     */
    public function testPick()
    {
        $person = ['firstname' => 'Test', 'lastname' => 'Kees'];
        $result = pick($person, ['firstname', 'age']);
        
        $this->assertArrayHasKey(
            'firstname',
            $result,
            'The result should contain the element with the given key'
        );
        $this->assertArrayNotHasKey(
            'lastname',
            $result,
            'The result should not contain the element that does not correspond to any of the given keys'
        );
        $this->assertArrayNotHasKey(
            'age',
            $result,
            'The result should not contain an element with a key that does not exist in the values array'
        );
        $this->assertEquals(
            ['firstname' => 'Test'],
            $result,
            'The result should be equal to the element that corresponds to the given key'
        );
    }
    
    public function testPickWithDefaultsAsArgument()
    {
        $person = ['firstname' => 'Test', 'lastname' => 'Kees'];
        $result = pick($person, ['firstname', 'age' ], ['Dummy', 24]);
        
        $this->assertEquals(
            24,
            $result['age'],
            'The key that is not present in values should be included in the result with the default value'
        );
        $this->assertArrayNotHasKey(
            'lastname',
            $result,
            'The result should not contain the element that does not correspond to any of the given keys'
        );
        $this->assertEquals(
            ['firstname' => 'Test', 'age' => 24],
            $result
        );
    }
    
    /**
     * @testdox pickValues should do the same as pick and replace all keys with numerical indexes
     * @depends testPick
     */
    public function testPickValues()
    {
        $person = ['firstname' => 'Test', 'lastname' => 'Kees'];
        $result = pickValues($person, ['firstname', 'age']);
        
        $this->assertArrayHasKey(
            0,
            $result,
            'The result should contain the element with the given key as the first element'
        );
        $this->assertArrayNotHasKey(
            'firstname',
            $result,
            'The result should not contain the non numerical key'
        );
        $this->assertCount(
            1,
            $result,
            'The result should not contain anymore elements than the keys that can be found in the values array'
        );
        $this->assertEquals(
            [0 => 'Test'],
            $result,
            'The result should be equal to the element that corresponds to the given key'
        );
    }
    
    /**
     * @testdox pickWithDefaults should do the same as pick and ensure that given 
     *          keys not present in the values array get a default value assigned
     * @depends testPick
     */
    public function testPickWithDefaults()
    {
        $person = ['firstname' => 'Test', 'lastname' => 'Kees'];
        $result = pickWithDefaults($person, ['firstname' => 'Dummy', 'age' => 24]);
        
        $this->assertEquals(
            24,
            $result['age'],
            'The key that is not present in values should be included in the result with the default value'
        );
        $this->assertArrayNotHasKey(
            'lastname',
            $result,
            'The result should not contain the element that does not correspond to any of the given keys'
        );
        $this->assertEquals(
            ['firstname' => 'Test', 'age' => 24],
            $result
        );
    }
    
    /**
     * @testdox pickValuesWithDefaults should do the same as pickWithDefaults and 
     *          replace all keys with numerical indexes
     * @depends testPickWithDefaults
     */
    public function testPickValuesWithDefaults()
    {
        $person = ['firstname' => 'Test', 'lastname' => 'Kees'];
        $result = pickValuesWithDefaults(
            $person,
            ['firstname' => 'Dummy', 'age' => 24]
        );
        
        $this->assertEquals(
            24,
            $result[1],
            'The key that is not present in values should be included in the result with the default value'
        );
        $this->assertArrayNotHasKey(
            'firstname',
            $result,
            'The result should not contain the non numerical key'
        );
        $this->assertCount(
            2,
            $result,
            'The result should not contain anymore elements than the keys that are searched for'
        );
        $this->assertEquals(
            [0 => 'Test', 1 => 24],
            $result
        );
    }
}