<?php

declare(strict_types=1);

namespace Skript\Utils\_Array;

use PHPUnit\Framework\TestCase;

use Skript\Utils\_Array;
use function Skript\Utils\_Array\{
    pick,
    pickValues,
    pickWithDefaults,
    pickValuesWithDefaults
};

final class _ArrayTest extends TestCase
{
    public function testStaticPick()
    {
        $person = ['firstname' => 'Test', 'lastname' => 'Kees'];
        $keys = ['firstname', 'age'];
        
        $this->assertEquals(
            pick($person, $keys),
            _Array::pick($person, $keys)
        );
    }
    
    public function testStaticPickValues()
    {
        $person = ['firstname' => 'Test', 'lastname' => 'Kees'];
        $keys = ['firstname', 'age'];
        
        $this->assertEquals(
            pickValues($person, $keys),
            _Array::pickValues($person, $keys)
        );
    }
    
    public function testStaticPickWithDefaults()
    {
        $person = ['firstname' => 'Test', 'lastname' => 'Kees'];
        $keys = ['firstname' => 'Dummy', 'age' => 24];
        
        $this->assertEquals(
            pickWithDefaults($person, $keys),
            _Array::pickWithDefaults($person, $keys)
        );
    }
    
    public function testStaticPickValuesWithDefault()
    {
        $person = ['firstname' => 'Test', 'lastname' => 'Kees'];
        $keys = ['firstname' => 'Dummy', 'age' => 24];
        
        $this->assertEquals(
            pickValuesWithDefaults($person, $keys),
            _Array::pickValuesWithDefaults($person, $keys)
        );
    }
}