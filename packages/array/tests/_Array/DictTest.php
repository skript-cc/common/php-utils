<?php

declare(strict_types=1);

namespace Skript\Utils\_Array;

use PHPUnit\Framework\TestCase;

use function Skript\Utils\_Array\{
    pick,
    pickValues,
    pickWithDefaults,
    pickValuesWithDefaults
};

final class DictTest extends TestCase
{
    public function testPick()
    {
        $person = ['firstname' => 'Test', 'lastname' => 'Kees'];
        $keys = ['firstname', 'age'];
        
        $this->assertEquals(
            pick($person, $keys),
            (new Dict($person))->pick($keys)->toArray()
        );
    }
    
    public function testPickWithDefaultsAsArguments()
    {
        $person = ['firstname' => 'Test', 'lastname' => 'Kees'];
        $defaults = ['firstname' => 'Dummy', 'age' => 24];
        
        $this->assertEquals(
            pickWithDefaults($person, $defaults),
            (new Dict($person))->pick(
                array_keys($defaults),
                array_values($defaults)
            )->toArray()
        );
    }
    
    public function testPickValues()
    {
        $person = ['firstname' => 'Test', 'lastname' => 'Kees'];
        $keys = ['firstname', 'age'];
        
        $this->assertEquals(
            pickValues($person, $keys),
            (new Dict($person))->pick($keys)->values()
        );
    }
    
    public function testPickWithDefaults()
    {
        $person = ['firstname' => 'Test', 'lastname' => 'Kees'];
        $defaults = ['firstname' => 'Dummy', 'age' => 24];
        
        $this->assertEquals(
            pickWithDefaults($person, $defaults),
            (new Dict($person))->pickWithDefaults($defaults)->toArray()
        );
    }
    
    public function testPickValuesWithDefaults()
    {
        $person = ['firstname' => 'Test', 'lastname' => 'Kees'];
        $defaults = ['firstname' => 'Dummy', 'age' => 24];
        
        $this->assertEquals(
            pickValuesWithDefaults($person, $defaults),
            (new Dict($person))->pickWithDefaults($defaults)->values()
        );
    }
    
    public function testPrepend()
    {
        $dict = new Dict(['b', 'c']);
        $this->assertEquals(
            ['a', 'b', 'c'],
            $dict->prepend('a')->toArray()
        );
    }
}