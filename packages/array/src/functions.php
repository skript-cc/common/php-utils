<?php

declare(strict_types=1);

namespace Skript\Utils\_Array;

/**
 * Pick all elements from an array with the given keys
 *
 * @example packages/array/examples/pick.php Example usage with extract
 *
 * @param array $values The values where to pick certain keys from
 * @param array $keys The keys to pick from values
 * @param array $defaults Default values
 *
 * @return array The elements from values which correspond to the given keys
 */
function pick(array $values, array $keys, array $defaults=[]): array
{
    $values = count($defaults) < 1
        ? $values
        : array_merge(
            array_combine($keys, $defaults),
            $values
        );
    return array_intersect_key(
        $values,
        array_fill_keys($keys, null)
    );
}

/**
 * Pick all elements from an array with the given keys and replaces all keys 
 * with numerical indexes
 *
 * @param array $values The values where to pick certain keys from
 * @param array $keys The keys to pick from values
 *
 * @return array    A numerical indexed array with the elements from values which 
 *                  correspond to the given keys
 */
function pickValues(array $values, array $keys): array
{
    return array_values(pick($values, $keys));
}

/**
 * Pick all elements from an array with the given keys, and sets a default value
 * if the element doesn't exist.
 *
 * @param array $values The values where to pick certain keys from
 * @param array $defaults Assoc array in the format ['key-to-pick' => 'default-value']
 *
 * @return array    The elements from values which correspond to the given keys or 
 *                  the default when the key doesn't exist in the values array
 */
function pickWithDefaults(array $values, array $defaults): array
{
    return pick(
        array_merge($defaults, $values),
        array_keys($defaults)
    );
}

/**
 * Same as pickWithDefaults, but returns all values with numerical indexes
 *
 * @example packages/array/examples/pickValuesWithDefaults.php Example usage with array destructuring
 *
 * @param array $values The values where to pick certain keys from
 * @param array $defaults Assoc array in the format ['key-to-pick' => 'default-value']
 *
 * @return array    A numerical indexed array with the elements from values which
 *                  correspond to the given keys or the default when the key
 *                  doesn't exist in the values array
 */
function pickValuesWithDefaults(array $values, array $defaults): array
{
    return array_values(pickWithDefaults($values, $defaults));
}

function stringifyKeys(array $input): array
{
    return array_combine(
        array_map(
            function ($key) { return '.'.$key; },
            array_keys($input)
        ),
        array_values($input)
    );
}

function mergeLists(...$lists): array
{
    return array_values(
        array_merge(
            ...array_map(__NAMESPACE__.'\stringifyKeys', $lists)
        )
    );
}