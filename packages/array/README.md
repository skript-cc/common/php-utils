# PHP utils - Array

An assorted collection of convenience array utilities.

## Notes

- Making class based interfaces for array utilities begs for an investigation
  into data types, such as sequences, sets, lists, tuples, maps, dictionaries,
  etc. Probably there are existing libraries available for this.