<?php

/**
 * Example Usage with extract
 */

use function Skript\Utils\_Array\pick;

$person = [
    'firstname' => 'Test',
    'lastname' => 'Kees',
    'age' => 23
];

extract(pick($person, ['firstname', 'lastname']));

echo "Hello {$firstname} {$lastname}!";

/**
 * The above example will output <code>Hello Test Kees!</code>
 */