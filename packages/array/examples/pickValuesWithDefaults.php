<?php

/**
 * Example usage with array destructuring
 */

use function Skript\Utils\_Array\pickValuesWithDefaults;

$person = [
    'firstname' => 'Test',
    'lastname' => 'Kees'
];

[$first, $last, $age] = pickValuesWithDefaults(
    $person, [
        'firstname' => null,
        'lastname' => null,
        'age' => -1
    ]
);

if ($age < 0) {
    echo "The age of {$first} {$last} is unknown.";
} else {
    echo "{$first} {$last} is ${age} years old.";
}

/**
 * The above example will output: <code>The age of Test Kees is unknown.</code>
 */