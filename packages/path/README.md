# PHP Utils - Path

Utilities for working with file and directory paths inspired by nodes 
[path module](https://nodejs.org/api/path.html).

## Functions

- `join()`
- `normalize()`
- `resolve()`
- `relative()`
- `isAbsolute()`
- `parse()`
- `format()`

## Contributing

Report issues and send pull requests in the [main repository]

[main repository]: https://gitlab.com/skript-cc/common/php-utils