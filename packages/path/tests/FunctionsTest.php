<?php

declare(strict_types=1);

namespace Skript\Utils\Path;

use PHPUnit\Framework\TestCase;

use function Skript\Utils\Path\{
    format,
    isAbsolute,
    join,
    normalize,
    parse,
    resolve,
    relative
};

final class FunctionsTest extends TestCase
{
    public function testJoin()
    {
        $this->assertEquals(
            '/part/of/a/path',
            join('/part', 'of', 'a', 'path'),
            'All path segments should be joined together to form a single path'
        );
        
        $this->assertEquals(
            '/part/of/path',
            join('/part', 'of', '', 'path'),
            'Zero length path segments should be ignored'
        );
        
        $this->assertEquals(
            '/part/of/path',
            join('/part', 'of', null, 'path'),
            'Null values should be ignored'
        );
    }
    
    public function testNormalize()
    {
        $this->assertEquals('/a/b/c', normalize('/a/b/c'), 'Normal paths should stay normal');
        $this->assertEquals('b/c', normalize('./b/c'), 'Single dots should be resolved');
        $this->assertEquals('b/c', normalize('b/./c'), 'Single dots in the middle should be resolved');
        $this->assertEquals('b/c', normalize('b/c/.'), 'Single dots at the end should be resolved');
        $this->assertEquals('/a/c', normalize('/a/b/../c'), 'Double dots should be resolved');
        $this->assertEquals('../../a/b', normalize('../../a/b'), 'Double dots at the beginning of a path should be retained');
        $this->assertEquals('/a/b/c', normalize('/a/b/c/'), 'Trailing slashes should be stripped');
    }
    
    public function testResolve()
    {
        $this->assertEquals('/foo/bar/baz', resolve('/foo/bar', './baz'));
        $this->assertEquals('/tmp/file', resolve('/foo/bar', '/tmp/file/'));
        $this->assertEquals(
            getcwd().'/wwwroot/static_files/gif/image.gif',
            resolve('wwwroot', 'static_files/png/', '../gif/image.gif')
        );
        $this->assertEquals('/bar/baz', resolve('/foo', '/bar', 'baz'));
        $this->assertEquals('/', resolve('/foo', '..'));
    }
    
    public function testRelative()
    {
        $cwdToRootCount = count(explode(DS, getcwd()));
        
        $this->assertEquals('..', relative('/a/b/c', '/a/b'), 'Relative path should go one directory up');
        $this->assertEquals('c', relative('/a/b', '/a/b/c'), 'Relative path should equal subdir');
        $this->assertEquals('b/c', relative('/a', '/a/b/c'), 'Relative path should equal subdir');
        $this->assertEquals(
            join('../..', getcwd(), 'd'),
            relative('/a/b', 'd/'),
            'When the to path is relative, it should be resolved to the current working directory'
        );
        $this->assertEquals(
            join(...array_merge(
                array_fill(0, $cwdToRootCount, '..'),
                ['a/b']
            )),
            relative('c/', '/a/b'),
            'When the from path is relative it should be resolved to the current working directory'
        );
    }
    
    public function testIsAbsolute()
    {
        $this->assertTrue(isAbsolute('/absolute/path'));
        $this->assertFalse(isAbsolute('relative/path'));
    }
    
    public function testParse()
    {
        $path = '/some/path/filename.ext';
        $this->assertEquals(pathinfo($path), parse($path));
    }
    
    public function testFormat()
    {
        $this->assertEquals(
            '/some/path/filename.ext',
            format(['dirname' => '/some/path', 'basename' => 'filename.ext'])
        );
        $this->assertEquals(
            '/some/path/filename.ext',
            format([
                'dirname' => '/some/path',
                'basename' => 'filename.ext',
                'filename' => 'file',
                'extension' => 'txt'
            ]),
            'basename should override filename and extension'
        );
        $this->assertEquals(
            '/some/path/file.txt',
            format([
                'dirname' => '/some/path',
                'filename' => 'file',
                'extension' => 'txt'
            ]),
            'filename and extension should be used when basename is not set'
        );
        $this->assertEquals(
            '/some/path/file',
            format([
                'dirname' => '/some/path',
                'filename' => 'file'
            ]),
            'extension should not be present'
        );
        $this->assertEquals(
            'file.txt',
            format([
                'filename' => 'file',
                'extension' => 'txt'
            ]),
            'without dirname we should be able to format a file'
        );
    }
}