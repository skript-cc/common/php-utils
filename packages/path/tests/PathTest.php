<?php

declare(strict_types=1);

namespace Skript\Utils;

use PHPUnit\Framework\TestCase;

use Skript\Utils\Path;
use function Skript\Utils\Path\{
    format,
    isAbsolute,
    join,
    normalize,
    parse,
    resolve,
    relative
};

final class PathTest extends TestCase
{
    public function testStaticJoin()
    {
        $this->assertEquals(
            join('/part', 'of', 'a', 'path'),
            Path::join('/part', 'of', 'a', 'path')
        );
    }
    
    public function testStaticNormalize()
    {
        $this->assertEquals(
            normalize('/a/b/../c'),
            Path::normalize('/a/b/../c')
        );
    }
    
    public function testStaticResolve()
    {
        $this->assertEquals(
            resolve('wwwroot', 'static_files/png/', '../gif/image.gif'),
            Path::resolve('wwwroot', 'static_files/png/', '../gif/image.gif')
        );
    }
    
    public function testStaticRelative()
    {
        $this->assertEquals(
            relative('/a/b/c', '/a/b'),
            Path::relative('/a/b/c', '/a/b')
        );
    }

    public function testStaticIsAbsolute()
    {
        $this->assertEquals(
            isAbsolute('/absolute/path'),
            Path::isAbsolute('/absolute/path')
        );
    }
    
    public function testStaticParse()
    {
        $path = '/some/path/filename.ext';
        $this->assertEquals(
            parse($path),
            Path::parse($path)
        );
    }

    public function testStaticFormat()
    {
        $this->assertEquals(
            format([
                'dirname' => '/some/path',
                'filename' => 'file',
                'extension' => 'txt'
            ]),
            Path::format([
                'dirname' => '/some/path',
                'filename' => 'file',
                'extension' => 'txt'
            ])
        );
    }
}