<?php

namespace Skript\Utils\Path;

const DS = DIRECTORY_SEPARATOR;

use function Skript\Utils\_Array\pickWithDefaults;

/**
 * Join path segments together to form a single path. Zero length and null path
 * segments are ignored.
 *
 * @param string Path segments to join
 * @return string The joined path segments
 */
function join(?string ...$paths): string
{
    $paths = array_values($paths);
    return implode(
        '/',
        array_filter(
            array_map(
                function ($key, ?string $segment) { 
                    return strlen($segment) > 0 
                        ? $key > 0 ? trim($segment, DS) : rtrim($segment, DS)
                        : null;
                },
                array_keys($paths),
                $paths
            )
        )
    );
}

/**
 * Normalizes the given path, resolving '..' and '.' segments.
 *
 * @param string The path to normalize
 * @return string The normalized path
 */
function normalize(string $path): string
{
    // strip out single dot symbols
    $path = preg_replace('|(?<!\.)\.'.DS.'|', '', $path);   // replace dot symbols at the start and in the middle
    $path = preg_replace('|'.DS.'\.$|', '', $path);         // replace dot symbols occuring at the end

    // resolve double dot symbols
    if (strpos($path, '..') > -1) {
        $parts = explode(DS, $path);
        $newPath = [];
        // retain dots at the beginning
        $newParts = [];
        $startPos=0;
        foreach ($parts as $i => $part) {
            if ($i==$startPos && $part == '..') {
                $startPos=$i+1;
                continue;
            }
            $newParts[] = $part;
        }
        // resolve the rest of the part
        foreach ($newParts as $i => $part) {
            if ($part === '..') {
                unset($newPath[count($newPath)-1]);
                continue;
            }
            $newPath[] = $part;
        }
        $pathSegments = array_merge(
            array_fill(0, $startPos, '..'), // recreate start parts
            $newPath // append resolved path
        );
        $path = count($pathSegments) === 1 && $pathSegments[0] === '' // check for absolute root paths
            ? DS
            : implode(DS, $pathSegments);
    }

    return $path === DS ? $path : rtrim($path, DS);
}

/**
 * Resolves a sequence of paths or path segments into an absolute path
 *
 * @param string The path segments to resolve
 * @return string The resolved absolute path
 */
function resolve(string ...$paths): string
{
    $pathsToJoin = [];
    foreach ($paths as $path) {
        if (isAbsolute($path)) {
            $pathsToJoin = [$path];
            continue;
        }
        $pathsToJoin[] = $path;
    }
    if (isset($pathsToJoin[0]) && !isAbsolute($pathsToJoin[0])) {
        array_splice($pathsToJoin, 0, 0, [getcwd()]);
    }
    return normalize(join(...$pathsToJoin));
}

/**
 * Returns the relative path from `$from` to `$to` based on the current working directory.
 *
 * @param string The source path to use as start location
 * @param string The target path where we want to end up
 * @return string A relative path from `$from` to `$to`
 */
function relative(string $from, string $to): string
{
    if (!isAbsolute($from)) {
        $from = join(getcwd(), $from);
    }
    
    if (!isAbsolute($to)) {
        $to = join(getcwd(), $to);
    }
    
    $srcParts = explode(DS, normalize($from));
    $targetParts = explode(DS, normalize($to));
    
    $offset = 0;
    $relPath = '';
    foreach ($srcParts as $i => $dir) {
        if (isset($targetParts[$i]) && $srcParts[$i] === $targetParts[$i]) {
            $offset++;
            continue;
        }
        $relPath .= '..'.DS;
    }

    return rtrim(
        $relPath . implode(DS, array_slice($targetParts, $offset)),
        DS
    );
}

/**
 * Check if path is absolute
 */
function isAbsolute(string $path): bool
{
    return $path[0] === DS;
}

/**
 * Alias for pathinfo
 */
function parse(string $path): array
{
    return pathinfo($path);
}

/**
 * Returns a path string from an associative array. This is the opposite of `parse()`.
 *
 * @param string $parts['dirname']
 * @param string $parts['basename']
 * @param string $parts['filename']
 * @param string $parts['extension']
 * @return string The formatted path
 */
function format(array $parts): string
{
    extract(
        pickWithDefaults(
            $parts, [
                'dirname' => null,
                'basename' => null,
                'filename' => null,
                'extension' => null
            ]
        )
    );
    
    if (!$basename) {
        $basename = $extension ? "$filename.$extension" : $filename;
    }
    
    return join($dirname, $basename);
}